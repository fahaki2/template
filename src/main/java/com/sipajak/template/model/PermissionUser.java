package com.sipajak.template.model;


import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "permission_user")
@Data
public class PermissionUser implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ROWID", nullable = true)
    String rowid;

    @Column(name = "permission_id")
    private Integer permissionId;
    @Column(name = "user_id")
    private Integer userId;

}
