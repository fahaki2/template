package com.sipajak.template.model;


import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "role_user")
@Data
public class RoleUser implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ROWID", nullable = true)
    String rowid;

    @Column(name = "role_id")
    private Integer roleId;
    @Column(name = "user_id")
    private Integer userId;

}
