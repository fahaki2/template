package com.sipajak.template.model.sso;

import com.sipajak.template.model.MasterUser;
import lombok.Data;

import java.io.Serializable;

@Data
public class UserAuth implements Serializable {
    private String access_token;
    private String token_type;
    private String refresh_token;
    private String expires_in;
    private String scope;
    private MasterUser masterUser;

}