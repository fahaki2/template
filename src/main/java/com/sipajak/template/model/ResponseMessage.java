package com.sipajak.template.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class ResponseMessage<T> implements Serializable {

    private Integer code;
    private String status;
    private String message;
    private T data;
}

