package com.sipajak.template.model;

import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "master_user")
@Data
@ToString
public class MasterUser implements Serializable {
    public MasterUser() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_seq")
    @SequenceGenerator(sequenceName = "user_seq", allocationSize = 1, name = "user_seq")
    @Column(name = "id")
    private Integer id;

    @Column(name = "username")
    private String username;
    @Column(name = "password")
    private String password;
    @Column(name = "email")
    private String email;
    @Column(name = "enabled")
    private boolean enabled;

    @Column(name = "status")
    private String status;
    @Column(name = "nama")
    private String nama;
    @Column(name = "notelp")
    private String notelp;
    @Column(name = "npwp")
    private String npwp;
    @Column(name = "efin")
    private String efin;
    @Column(name = "alamat")
    private String alamat;
    @Column(name = "kode_pos")
    private String kode_pos;
    @Column(name = "handphone")
    private String handphone;
    @Column(name = "fax")
    private String fax;
    @Column(name = "klu")
    private String klu;
    @Column(name = "id_paket")
    private Integer id_paket;

    @Column(name = "created")
    private LocalDateTime created;
    @Column(name = "modified")
    private LocalDateTime modified;
    @Column(name = "last_login")
    private LocalDateTime last_login;
    @Column(name = "linkaproval")
    private String linkaproval;

    @Column(name = "id_parent_group")
    private Integer id_parent_group;
    @Column(name = "id_parent_admin")
    private Integer id_parent_admin;

    @Column(name = "jabatan")
    private String jabatan;

    @Column(name = "nib_badan")
    private String nibBadan;

    @Column(name = "kontak_badan")
    private String kontakBadan;

    @Column(name = "web_badan")
    private String webBadan;

    @Column(name = "jenis_user")
    private String jenisUser;

    @Column(name = "approved_by")
    private String approvedBy;

    @Column(name = "last_change_password_date")
    private LocalDateTime lastChangePasswordDate;

    @Transient
    private String refresh_token;

    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "role_user", joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {
                    @JoinColumn(name = "role_id", referencedColumnName = "id")})
    private List<Role> roles;

    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    @JoinTable(name = "permission_user", joinColumns = {@JoinColumn(name = "user_id", referencedColumnName = "id")},
            inverseJoinColumns = {
                    @JoinColumn(name = "permission_id", referencedColumnName = "id")})
    private List<Permission> permission;

}
