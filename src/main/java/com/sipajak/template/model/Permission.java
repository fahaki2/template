package com.sipajak.template.model;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "permission")
@Data
public class Permission implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "permission_seq")
    @SequenceGenerator(sequenceName = "permission_seq", allocationSize = 1, name = "permission_seq")
    @Column(name = "id")
    private Integer id;
    @Column(name = "name")
    private String name;
}

