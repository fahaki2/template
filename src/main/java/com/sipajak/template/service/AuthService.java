package com.sipajak.template.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.sipajak.template.config.CustomUserTokenService;
import com.sipajak.template.model.MasterUser;
import com.sipajak.template.model.sso.UserAuth;
import org.bouncycastle.util.encoders.Base64;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.AccessTokenConverter;
import org.springframework.security.oauth2.provider.token.DefaultAccessTokenConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestOperations;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

@Component
public class AuthService {

    @Value("${sso.url}")
    String authServer;

    @Value("${redirect-uri}")
    String redirectUri;

    @Value("${security.oauth2.client.access-token-uri}")
    String accessTokenUri;

    @Value("${security.oauth2.client.client-id}")
    String clientId;

    @Value("${security.oauth2.client.client-secret}")
    String clientSecret;

    @Autowired
    CustomUserTokenService customUserTokenService;

    @Autowired
    ObjectMapper geObjMapper;

    private RestOperations restTemplate;

    private AccessTokenConverter tokenConverter = new DefaultAccessTokenConverter();

    public void setAccessTokenConverter(AccessTokenConverter accessTokenConverter) {
        this.tokenConverter = accessTokenConverter;
    }

    public void setRestTemplate(RestOperations restTemplate) {
        this.restTemplate = restTemplate;
    }

    public AuthService() {
        restTemplate = new RestTemplate();
        ((RestTemplate) restTemplate).setErrorHandler(new DefaultResponseErrorHandler() {
            @Override
            // Ignore 400
            public void handleError(ClientHttpResponse response) throws IOException {
                if (response.getRawStatusCode() != 400) {
                    super.handleError(response);
                }
            }
        });
    }

    final ObjectMapper mapper = new ObjectMapper(); // jackson's objectmapper

    private static final Logger log = LoggerFactory.getLogger(AuthService.class);

    public void getToken(String code, HttpSession objSession) {
        try {
            UserAuth userAuth = new UserAuth();

            MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();
            formData.add("grant_type", "authorization_code");
            formData.add("code", code);
            formData.add("redirect_uri", redirectUri);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.set("Authorization", getAuthorizationHeader(clientId, clientSecret));
            HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(formData, headers);

            HttpEntity<String> respon = restTemplate.exchange(accessTokenUri,
                    HttpMethod.POST, request, String.class);

            Gson gson = new Gson();
            userAuth = gson.fromJson(respon.getBody(), UserAuth.class);

            setOauthForLoadAuthentication(objSession, userAuth);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public UserAuth loadAuthWithRefreshtoken(HttpSession session, String refresh_token) {
        MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();
        formData.add("grant_type", "refresh_token");
        formData.add("refresh_token", refresh_token);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.set("Authorization", customUserTokenService.getAuthorizationHeader(clientId, clientSecret));
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(formData, headers);
        UserAuth userAuth = restTemplate.exchange(accessTokenUri, HttpMethod.POST, request, UserAuth.class).getBody();
        setOauthForLoadAuthentication(session, userAuth);
        return userAuth;
    }

    public void setOauthForLoadAuthentication(HttpSession session, UserAuth userAuth) {
        try {
            OAuth2Authentication oAuth2Authentication = customUserTokenService.loadAuthentication(userAuth.getAccess_token());
            SecurityContextHolder.getContext().setAuthentication(oAuth2Authentication);
            session.isNew();
            session.setAttribute("userAuth", userAuth);

            MasterUser masterUser = geObjMapper.convertValue(oAuth2Authentication.getPrincipal(), MasterUser.class);
            session.setAttribute("userProfile", masterUser);
        } catch (Exception e) {

        }
    }

    private String getAuthorizationHeader(String clientId, String clientSecret) {
        String creds = String.format("%s:%s", clientId, clientSecret);
        try {
            return "Basic " + new String(Base64.encode(creds.getBytes("UTF-8")));
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException("Could not convert String");
        }
    }
}
