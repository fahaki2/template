package com.sipajak.template.controller;

import com.sipajak.template.config.AccessToken;
import com.sipajak.template.config.CustomUserTokenService;
import com.sipajak.template.model.MasterUser;
import com.sipajak.template.model.sso.UserAuth;
import com.sipajak.template.service.AuthService;
import com.sipajak.template.util.SessionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.WebUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Controller
public class AuthController {

    @Value("${sso.url}")
    String authServer;

    @Value("${security.oauth2.client.access-token-uri}")
    String accessTokenUri;

    @Value("${security.oauth2.client.client-id}")
    String clientId;

    @Value("${security.oauth2.client.client-secret}")
    String clientSecret;

    @Autowired
    SessionUtil sessionUtil;

    @Autowired
    CustomUserTokenService customUserTokenService;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    AuthService oauthService;

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @RequestMapping(value = "/auth/cek", method = RequestMethod.GET)
    public void cek(@RequestParam(required = false) String code,
                    RedirectAttributes redirectAttributes, HttpSession sessionObj, HttpServletResponse response) throws IOException {
        if (!code.isEmpty()) {
            //tukarkan code dengan access_token
            //bawa access_token untuk akses halaman yang dipagari dengan oauth
            oauthService.getToken(code, sessionObj);
            //secService.updateSecurityContextHolder(sessionObj);
            UserAuth userAuth = (UserAuth) sessionObj.getAttribute("userAuth");
            if (userAuth.getAccess_token() != null) {
                response.sendRedirect("/home");
            } else {
                response.sendRedirect("/auth/login");
            }
        } else {
            response.sendRedirect("/auth/login");
        }
    }

    @GetMapping(value = "/auth/login")
    public String login(HttpServletRequest request, HttpServletResponse response) {
        sessionUtil.setSessionUser(request);
        HttpSession session = request.getSession();
        MasterUser sesMasterUser = (MasterUser) session.getAttribute("userProfile");
        if (sesMasterUser == null) {
            session.removeAttribute("userProfile");
            session.invalidate();
        }
        return "redirect:/home";
    }

    @RequestMapping(value = "/auth/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        sessionUtil.setSessionUser(request);
        HttpSession session = request.getSession();
        MasterUser sesMasterUser = (MasterUser) session.getAttribute("userProfile");
        if (sesMasterUser != null) {
            String redirectAuthLogout = "";
            try {
                redirectAuthLogout = "redirect:" + authServer + "/logout?token=" + (AccessToken.getAccessToken()).replace("Bearer", "").replace("bearer", "").trim();
            }catch (Exception e){
                try {
                    UserAuth userAuth = (UserAuth) session.getAttribute("userAuth");
                    redirectAuthLogout = "redirect:" + authServer + "/logout?token=" + userAuth.getAccess_token();
                }catch (Exception ex){
                    response.addHeader("Clear-Site-Data", "\"cookies\"");
                    return "redirect:/auth/login";
                }
            }
            session.invalidate();
            response.addHeader("Clear-Site-Data", "\"cookies\"");
            return redirectAuthLogout;
        } else {
            return "redirect:/auth/login";
        }
    }

    @RequestMapping(value = "/auth/cek/refresh_token")
    public String refreshToken(HttpServletRequest request, HttpServletResponse response) {
        Authentication a = SecurityContextHolder.getContext().getAuthentication();
        MasterUser sesMasterUser = null;
        HttpSession session = request.getSession();

        if (a != null) {
            if (!a.getPrincipal().equals("anonymousUser")) {

                sessionUtil.setSessionUser(request);
                sesMasterUser = (MasterUser) session.getAttribute("userProfile");
            }
        }

        String valueRememberme = "";
        try {
            Cookie rememberme = WebUtils.getCookie(request, "REFRESH-TOKEN_" + clientId);
            valueRememberme = rememberme.getValue();
        }catch (Exception e){
//            e.printStackTrace();
        }

        if (sesMasterUser == null) {
            if (!valueRememberme.equals("")) {
                oauthService.loadAuthWithRefreshtoken(session, valueRememberme);
            }
        } else {
            if (valueRememberme.equals("")) {
                if (sesMasterUser.getRefresh_token() != null) {
                    Cookie newrememberme = new Cookie("REFRESH-TOKEN_" + clientId, sesMasterUser.getRefresh_token());
                    newrememberme.setPath(request.getContextPath());
                    newrememberme.setMaxAge(30 * 24 * 60 * 60); // diset cookie 30 hari
                    response.addCookie(newrememberme);
                }
            }
        }
        return "redirect:/auth/login";
    }
}
