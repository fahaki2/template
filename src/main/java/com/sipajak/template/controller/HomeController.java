package com.sipajak.template.controller;

import com.sipajak.template.model.MasterUser;
import com.sipajak.template.model.sso.UserAuth;
import com.sipajak.template.util.SessionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.ResourceServerProperties;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class HomeController {

    @Autowired
    private ResourceServerProperties sso;

    @Autowired
    SessionUtil sessionUtil;

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @GetMapping(value = {"", "/"})
    @PreAuthorize("hasAnyRole('client','perekam')  AND hasAuthority('app_filing')")
    public ModelAndView index(HttpServletRequest request, HttpServletResponse response, Model model) {
        return new ModelAndView(new RedirectView("/home", true, false));
    }

    @GetMapping(value = "/home")
    @PreAuthorize("hasAnyRole('client','perekam')  AND hasAuthority('app_filing')")
    public ModelAndView home(HttpServletRequest request, HttpServletResponse response, Model model) {
        sessionUtil.setSessionUser(request);
        HttpSession session = request.getSession();
        MasterUser sesMasterUser = (MasterUser) session.getAttribute("userProfile");
        UserAuth userAuth = (UserAuth) session.getAttribute("userAuth");

        if (sesMasterUser.getRefresh_token() != null) {
            Cookie rememberme = new Cookie("REFRESH-TOKEN_" + sso.getClientId(), sesMasterUser.getRefresh_token());
            rememberme.setPath(request.getContextPath());
            rememberme.setMaxAge(30 * 24 * 60 * 60); // diset cookie 30 hari
            response.addCookie(rememberme);
        }

        return new ModelAndView("home");
    }

}
