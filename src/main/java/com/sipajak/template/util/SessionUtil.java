package com.sipajak.template.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sipajak.template.model.MasterUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Component
public class SessionUtil {

    @Autowired
    ObjectMapper geObjMapper;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    public void setSessionUser(HttpServletRequest request) {
        HttpSession session = request.getSession();
        if (session.getAttribute("userProfile") == null || session.getAttribute("userProfile").equals("")) {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            MasterUser sesMasterUser = geObjMapper.convertValue(auth.getPrincipal(), MasterUser.class);
            session.setAttribute("userProfile", sesMasterUser);
        }

    }
}
